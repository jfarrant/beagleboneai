import numpy as np
import matplotlib.pyplot as plt

def read_telemetry_data(filename):
    log_dict = {}
    tlog = open(filename, 'r')
    lines = tlog.readlines()

    for line in lines:
        line_split = line.rstrip('\n').split(',')
        if line_split[0] in log_dict.keys():
            entry = log_dict[line_split[0]]
            for i in range(1, len(line_split)):
                if line_split[i] == 'True':
                    entry[i - 1] = np.append(entry[i - 1], True)
                elif line_split[i] == 'False':
                    entry[i - 1] = np.append(entry[i - 1], False)
                else:
                    entry[i - 1] = np.append(entry[i - 1], float(line_split[i]))
        else:
            entry = []
            for i in range(1, len(line_split)):
                if line_split[i] == 'True':
                    entry.append(np.array(True))
                elif line_split[i] == 'False':
                    entry.append(np.array(False))
                else:
                    entry.append(np.array(float(line_split[i])))

        log_dict[line_split[0]] = entry
    return log_dict

t_log = read_telemetry_data("TLog.txt")

# Time is always the first entry in the list
#time_GPS = t_log['MsgID.GLOBAL_POSITION'][0][:]
#longitude = t_log['MsgID.GLOBAL_POSITION'][1][:]
#latitude = t_log['MsgID.GLOBAL_POSITION'][2][:]

time_local_position = t_log['MsgID.RAW_GYROSCOPE'][0][:]
x_local_position = t_log['MsgID.RAW_GYROSCOPE'][1][:]
y_local_position = t_log['MsgID.RAW_GYROSCOPE'][2][:]
z_local_position = t_log['MsgID.RAW_GYROSCOPE'][3][:]



fig, axs = plt.subplots(3, 3, sharex = False, sharey=False)
fig.suptitle('MTU TELEMETRY DATA')

axs[0, 0].plot(time_local_position, x_local_position)
axs[0, 0].plot(time_local_position, y_local_position)
axs[0, 0].plot(time_local_position, z_local_position)
axs[0, 0].set(xlabel='time (S)', ylabel= 'Raw Gyro Data (m)')
axs[0, 0].legend(['x','y','z'])
axs[0, 0].grid(True, which='both', axis='both')

##axs[0, 1].plot(time_GPS, longitude)
##axs[0, 1].plot(time_GPS, latitude)
##axs[0, 1].set(xlabel='Time (S)', ylabel='GPS Coordinates')
##axs[0, 1].legend(['Longitude','Latitude'])
##axs[0, 1].grid(True, which='both', axis='both')

##axs[1, 0].plot(hours, node1Capacity)
##axs[1, 0].set(xlabel='Time (Hours)', ylabel='Capacity (Ampere hours)')
##axs[1, 0].grid(True, which='both', axis='both')
##
##axs[1, 1].plot(hours, node1RemainingLife)
##axs[1, 1].set(xlabel='Time (Hours)', ylabel='Battery Life %')
##axs[1, 1].grid(True, which='both', axis='both')
##
##axs[2, 0].plot(hours, inputCurrent)
##axs[2, 0].plot(hours, currentLimit, color='r')
##axs[2, 0].set(xlabel='Time (Hours)', ylabel='Input Current (Amps)')
##axs[2, 0].legend(['Input Current', 'Battery Input Current Limit'])
##axs[2, 0].grid(True, which='both', axis='both')
##
##axs[2, 1].plot(hours, outputCurrent)
##axs[2, 1].plot(hours, currentLimit, color='r')
##axs[2, 1].set(xlabel='Time (Hours)', ylabel='Output Current (Amps)')
##axs[2, 1].legend(['Output Current', 'Battery Output Current Limit'])
##axs[2, 1].grid(True, which='both', axis='both')

plt.tight_layout()
plt.show()

##MsgID.STATE: self._update_state,
##MsgID.GLOBAL_POSITION: self._update_global_position,
##MsgID.LOCAL_POSITION: self._update_local_position,
##MsgID.GLOBAL_HOME: self._update_global_home,
##MsgID.LOCAL_VELOCITY: self._update_local_velocity,
##MsgID.RAW_GYROSCOPE: self._update_gyro_raw,
##MsgID.RAW_ACCELEROMETER: self._update_acceleration_raw,
##MsgID.BAROMETER: self._update_barometer,
##MsgID.ATTITUDE: self._update_attitude
