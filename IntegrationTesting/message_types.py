import numpy as np
import time

class DepthSensorMessage(object):
    """Message for depth sensor (e.g. pressure) information
    the properties of and measurement from a given depth sensor onboard
    the MTU.
    Attributes:
        _min_distance: minimum detectable distance in feet
        _max_distance: maximum detectable distance in feet
        _measurement: the depth measured in feet (greater number == greater depth)
    """

    def __init__(self, min_depth, max_depth, measurement):
        self._min_depth = min_depth
        self._max_depth = max_depth
        self._measurement = measurement
        self._time = time.time()

    @property
    def measurement(self, data):
        """
        (float): tuple containing the measurement information defined as
        (depth)
        """
        time = self._updateTime
        return time

    @property
    def properties(self):
        """ (float, float): tuple containing the properties of the sensor defined as (min distance, max distance) """
        return (self._min_depth, self._max_depth)

    @property
    def time(self):
        return self._time

class GyroMessage(object):
    def __init__(self):
        self._x = 0.0
        self._y = 0.0
        self._z = 0.0
        self._time = 0.0
        self._data = {'time' : self._time,'x' : self._x, 'y' : self._y, 'z' : self._z}
        

    def _update(self, data):
        self._time = data[0]
        self._data['time'] = data[0]
        self._x = data[1]
        self._data['x'] = data[1]
        self._y = data[2]
        self._data['y'] = data[2]
        self._z = data[3]
        self._data['z'] = data[3]
        

    @property
    def time(self):
        return self._time
    @property
    def x(self):
        return self._x
    @property
    def y(self):
        return self._y
    @property
    def z(self):
        return self._z
    @property
    def data(self):
        return self._data
        
